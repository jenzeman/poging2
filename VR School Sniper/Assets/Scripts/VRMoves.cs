﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UnityEngine.UI;

public class VRMoves : MonoBehaviour
{
    public GameObject player;
    private IEnumerator timerMove;
    private IEnumerator timerQuit;

    // Start is called before the first frame update
    void Awake()
    {
        timerMove = TimerCoroutine();
        timerQuit = TimerQuitCoroutine();

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ShowText()
    {
        this.GetComponentInChildren<Canvas>().enabled = true;
    }

    public void HideText()
    {
        this.GetComponentInChildren<Canvas>().enabled = false;
    }
    public void MoveToLocation()
    {

        UnityEngine.Debug.Log("looking at " + this.name);
        this.GetComponent<Renderer>().material.color = Color.red;
        StartCoroutine(timerMove);


    }

    public void CancelMoveToLocation()
    {
        StopCoroutine(timerMove);
        timerMove = TimerCoroutine();
        UnityEngine.Debug.Log("Stopped coroutine");
        this.GetComponent<Renderer>().material.color = Color.white;
        

    }

    public void QuitApp()
    {
        UnityEngine.Debug.Log("looking at " + this.name);
        this.GetComponent<Image>().color = Color.red;
        StartCoroutine(timerQuit);
    }

    public void CancelQuitApp()
    {
        StopCoroutine(timerQuit);
        timerQuit = TimerQuitCoroutine();
        UnityEngine.Debug.Log("Stopped coroutine");
        this.GetComponent<Image>().color = Color.white;
    }

    IEnumerator TimerCoroutine()
    {        
        UnityEngine.Debug.Log("Waiting...");
        yield return new WaitForSeconds(2);
        UnityEngine.Debug.Log("Teleporting ...");
        player.transform.position = new Vector3(this.transform.position.x, player.transform.position.y, this.transform.position.z);
    }

    IEnumerator TimerQuitCoroutine()
    {
        UnityEngine.Debug.Log("Waiting...");
        yield return new WaitForSeconds(2);
        UnityEngine.Debug.Log("quitting ...");
        Application.Quit();
    }
}
