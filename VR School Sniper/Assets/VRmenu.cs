﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRmenu : MonoBehaviour
{
    public Camera player;
    public Canvas QuitMenu;

    // Start is called before the first frame update
    void Start()
    {
        QuitMenu.GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rotationVector = player.transform.rotation.eulerAngles;
        rotationVector.z = 0;
        rotationVector.x = 0;
        this.transform.rotation = Quaternion.Euler(rotationVector);

        if (player.transform.rotation.eulerAngles.x >= 40)
        {
            QuitMenu.enabled = true;
        }
        else
        {
            QuitMenu.enabled = false;
        }
    }
}
