﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsSniper : MonoBehaviour
{
    public static bool isSniper = false;
    // Start is called before the first frame update

    
    public void SniperToggle()
    {
        if (!isSniper)
        {
        isSniper = true;
        Debug.Log("Sniper Toggle is true."); // play as Sniper
        }
        else
        {
            isSniper = false;
            Debug.Log("Sniper Toggle is false."); //play as Infiltrant
        }

    }
}
