﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviourPun
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public GameObject result;
    Material playerColor;
    bool isSniper = false;
    public GameObject interactingOverlay;
    GameObject progressbar;
    GameObject succesMessage;
    Slider progressbarSlider;
    Text succesMessageText;
    GameObject doubleAgentImgObject;
    Image doubleAgentImg;
    bool hasPassword = false;
    bool hasData = false;

    private void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        if (photonView.IsMine)
        {
            PlayerManager.LocalPlayerInstance = this.gameObject;
            this.GetComponentInChildren<Camera>().enabled = true;
            this.GetComponentInChildren<AudioListener>().enabled = true;

            if (this.GetComponentInChildren<MouseLookScript>())
            {
                this.GetComponentInChildren<MouseLookScript>().enabled = true;
            }


            if (this.GetComponentInChildren<Canvas>())
            {
                this.GetComponentInChildren<Canvas>().enabled = true;
                result.SetActive(true);
            }

            //Overlay for player
            if (this.GetComponent<Rigidbody>())
            {
                Instantiate(interactingOverlay);
                progressbar = GameObject.Find("ProgressBar");
                progressbar.SetActive(false);
                progressbarSlider = progressbar.GetComponent<Slider>();
                succesMessage = GameObject.Find("SuccesMessage");
                succesMessage.SetActive(false);
                succesMessageText = succesMessage.GetComponent<Text>();
                doubleAgentImgObject = GameObject.Find("DoubleAgentImage");
                doubleAgentImg = doubleAgentImgObject.GetComponent<Image>();


            }
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Rigidbody>())//Checks if the local player instance is a player or sniper (a player has a rigid body, a sniper does not)
        {
            //Apply a color to the player
            playerColor = GameObject.Find("AI manager").GetComponent<AIManager>().playerMaterial;
            this.GetComponentInChildren<SkinnedMeshRenderer>().material = playerColor;

            //Display color of double agent
            if (doubleAgentImg != null)
            {
                doubleAgentImg.color = GameObject.Find("AI manager").GetComponent<AIManager>().doubleAgentColor;
            }

            //(De)activate interaction overlay
            if (this.GetComponent<charController>().isInteracting)
            {
                progressbar.SetActive(true);
                progressbarSlider.value++;
                if (progressbarSlider.value >= 300) //If interaction was succesfull
                {
                    succesMessage.SetActive(true);

                    switch (this.GetComponent<charController>().target)
                    {
                        case "computer":
                            if (hasPassword)
                            {
                                succesMessageText.color = Color.green;
                                succesMessageText.text = "Aquired data!";
                                hasData = true;
                            }
                            else
                            {
                                succesMessageText.color = Color.red;
                                succesMessageText.text = "Password required!";
                            }

                            break;
                        case "fireAlarm":
                            if (hasData)
                            {
                                succesMessageText.color = Color.green;
                                succesMessageText.text = "Firealarm activated!";
                            }
                            else
                            {
                                succesMessageText.color = Color.red;
                                succesMessageText.text = "Need to collect the data first!";
                            }
                            break;
                        case "doubleAgent":
                            succesMessageText.color = Color.green;
                            succesMessageText.text = "Password aquired!";
                            hasPassword = true;
                            break;
                        default:
                            break;
                    }
                }

            }
            if (!this.GetComponent<charController>().isInteracting)
            {
                if (progressbar != null)
                {
                progressbar.SetActive(false);
                succesMessage.SetActive(false);
                progressbarSlider.value = 0;
                }

            }
        }

    }
}
