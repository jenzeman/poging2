﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MouseLookScript : MonoBehaviour
{
    private float X;
    private float Y;

    public float minFov = 15f;
    public float maxFov = 90f;
    public float scrollSensitivity = 10f;
    public float mouseSensitivity;
    private float fov;
    public Camera playerCam;
    public static bool lookingAtDoubleAgent = false;
    public static Collider hitCol = null;
    public GameObject sniperWin, sniperLose;



    void Awake()
    {
        Vector3 euler = transform.rotation.eulerAngles;
        X = euler.x;
        Y = euler.y;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {


        if (MouseLookScript.hitCol != null)
        {
            // checking what was hit and showing result.
            if (MouseLookScript.hitCol.ToString() == "character(Clone) (UnityEngine.CharacterController)")
            {
                Time.timeScale = 0.5f;
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
                playerCam.GetComponentInChildren<Canvas>().enabled = false;
                sniperWin.SetActive(true);

            }
            else if (MouseLookScript.hitCol.ToString() == "AI (2) (UnityEngine.BoxCollider)" || MouseLookScript.hitCol.ToString() == "AI (1) (UnityEngine.BoxCollider)" || MouseLookScript.hitCol.ToString() == "DoubleAgent (UnityEngine.BoxCollider)")
            {
                Time.timeScale = 0.5f;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                playerCam.GetComponentInChildren<Canvas>().enabled = false;
                sniperLose.SetActive(true);

            }
        }

        if (!IsUsingArduino.isUsingArduino) //Keyboard controls
        {

            //looking around
            const float MIN_X = 0.0f;
            const float MAX_X = 360.0f;
            const float MIN_Y = -90.0f;
            const float MAX_Y = 90.0f;

            X += Input.GetAxis("Mouse X") * (mouseSensitivity * Time.deltaTime);
            if (X < MIN_X) X += MAX_X;
            else if (X > MAX_X) X -= MAX_X;
            Y -= Input.GetAxis("Mouse Y") * (mouseSensitivity * Time.deltaTime);
            if (Y < MIN_Y) Y = MIN_Y;
            else if (Y > MAX_Y) Y = MAX_Y;

            transform.rotation = Quaternion.Euler(Y, X, 0.0f);

            fov = playerCam.fieldOfView;
            fov -= Input.GetAxis("Mouse ScrollWheel") * scrollSensitivity;
            fov = Mathf.Clamp(fov, minFov, maxFov);
            playerCam.fieldOfView = fov;



            if (Input.GetMouseButtonDown(0))
            {

                Debug.Log("shot fired");
                Shoot();
            }

        }
        else  //Arduino controls
        {
            //looking around
            const float MIN_X = 0.0f;
            const float MAX_X = 360.0f;
            const float MIN_Y = -90.0f;
            const float MAX_Y = 90.0f;

            X += SerialCommThreaded.lookXLvl * (mouseSensitivity * Time.deltaTime);
            if (X < MIN_X) X += MAX_X;
            else if (X > MAX_X) X -= MAX_X;
            Y -= SerialCommThreaded.lookYLvl * (mouseSensitivity * Time.deltaTime);
            if (Y < MIN_Y) Y = MIN_Y;
            else if (Y > MAX_Y) Y = MAX_Y;

            transform.rotation = Quaternion.Euler(Y, X, 0.0f);

            fov = playerCam.fieldOfView;
            //fov -= SerialCommThreaded.zoomLvl;
            fov = SerialCommThreaded.zoomLvl;
            fov = Mathf.Clamp(fov, minFov, maxFov);
            playerCam.fieldOfView = fov;


            Ray ray = playerCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "DoubleAgent")
                {
                    lookingAtDoubleAgent = true;
                }
                else
                {
                    lookingAtDoubleAgent = false;
                }
            }
            else
            {
                lookingAtDoubleAgent = false;
            }

            if (SerialCommThreaded.shotFired)
            {
                Debug.Log("shot fired");
                Shoot();
                SerialCommThreaded.shotFired = false;
            }

        }
    }

    // Shoot method
    void Shoot()
    {

        Ray rayShot = playerCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit shot;
        RaycastHit hitInfo;

        if (Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hitInfo, LayerMask.GetMask("Character")))
        {
            hitCol = hitInfo.collider; // What was hit.
            Debug.Log(" + " + hitCol);
        }
    }

}

