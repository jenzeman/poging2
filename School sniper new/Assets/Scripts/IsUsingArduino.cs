﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsUsingArduino : MonoBehaviour
{
    public static bool isUsingArduino = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ArduinoToggle()
    {
  
        if (!isUsingArduino)
        {
            isUsingArduino = true;
            Debug.Log("ArduinoToggle is true."); // play with arduino
        }
        else
        {
            isUsingArduino = false;
            Debug.Log("ArduinoToggle is false."); //play with keyboard
        }
    }
}
