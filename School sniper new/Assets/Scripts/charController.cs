﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Photon.Pun;

public class charController : MonoBehaviourPun
{
    float speed = 4;
    float rotSpeed = 120;
    float rotation = 0;
    float gravity = 8;
    bool isAbleToInteract = false;
    bool isAbleToTalk = false;
    public bool isInteracting = false;
    public string target;
    public GameObject characterWin, characterLose;

    Vector3 moveDir = Vector3.zero;

    CharacterController controller;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
        {
            return;
        }

        if (controller.isGrounded)
        {
            if (Input.GetKey(KeyCode.UpArrow)) //Walk forward
            {
                anim.SetInteger("condition", 1); //Activate walk animation
                moveDir = new Vector3(0, 0, 1);
                moveDir *= speed;
                moveDir = transform.TransformDirection(moveDir);
            }
            if (Input.GetKeyUp(KeyCode.UpArrow)) //Stop walking
            {
                anim.SetInteger("condition", 0); //Activate idle animation
                moveDir = new Vector3(0, 0, 0);
            }
            if (Input.GetKey(KeyCode.LeftArrow)) //rotate left
            {
                rotation -= 1 * rotSpeed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.RightArrow)) //rotate right
            {
                rotation += 1 * rotSpeed * Time.deltaTime;
            }

        }
        transform.eulerAngles = new Vector3(0, rotation, 0);
        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * Time.deltaTime);

        //Interacting
        if (isAbleToInteract)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (isAbleToTalk)
                {
                    anim.SetBool("isTalking", true);
                }
                else
                {
                    anim.SetBool("isInteracting", true);
                }

                isInteracting = true;

            }
            if (!Input.GetKey(KeyCode.Mouse0))
            {
                anim.SetBool("isInteracting", false);
                anim.SetBool("isTalking", false);
                isInteracting = false;
            }
        }
        if (!isAbleToInteract)
        {
            anim.SetBool("isInteracting", false);
            anim.SetBool("isTalking", false);
            isInteracting = false;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        int tag = 4; // 4 or higher means that the target is not interactable
        if (!int.TryParse(other.tag, out tag))
        {
            //Debug.Log("Parse failed");
            tag = 4;
        }
        if (tag <= 3 || tag == 26) //Checks of object is interactable
        {
            isAbleToInteract = true;

            if (tag == 26)
            {
                isAbleToTalk = true;
            }

            switch (tag)
            {
                case 0:
                    target = "computer";
                    break;
                case 1:
                    target = "fireAlarm";
                    break;
                case 2:
                    target = "fireAlarm";
                    break;
                case 3:
                    target = "fireAlarm";
                    break;
                case 26:
                    target = "doubleAgent";
                    break;

                default:
                    break;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        int tag = 4; // 4 or higher means that the target is not interactable
        if (!int.TryParse(other.tag, out tag))
        {
            //Debug.Log("Parse failed");
            tag = 4;
        }
        if (tag <= 3 || tag == 26) //Checks of object is interactable
        {
            isAbleToInteract = false;
            isAbleToTalk = false;
        }
    }
}
