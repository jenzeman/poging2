﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperController : MonoBehaviour
{
    float camAngle = 0;
    public GameObject camAnchor;
    public int minAngle, maxAngle;
    public float turnSpeed;


    // Update is called once per frame
    void Update()
    {
        if (!IsUsingArduino.isUsingArduino)  // Keyboard controls
        {
            //Rotate camera
            if (Input.GetKey(KeyCode.Q))
            {
                if (camAngle <= 75)
                {
                    camAngle++;
                    camAnchor.transform.localRotation = Quaternion.Euler(0, camAngle, 0);
                }
            }
            if (Input.GetKey(KeyCode.D))
            {
                if (camAngle >= -75)
                {
                    camAngle--;
                    camAnchor.transform.localRotation = Quaternion.Euler(0, camAngle, 0);
                }
            }
        }
        else //Arduino controls
        {
            // rotate camera    
            if (SerialCommThreaded.turnLeft)
            {
                float turnAngle = turnSpeed * Time.deltaTime;
                camAngle += turnAngle;
                camAngle = Mathf.Clamp(camAngle, minAngle, maxAngle);
                Debug.Log(camAngle + " CamAngle");
                camAnchor.transform.localRotation = Quaternion.Euler(0, camAngle, 0);

            }

            if (SerialCommThreaded.turnRight)
            {
                float turnAngle = turnSpeed * Time.deltaTime;
                camAngle -= turnAngle;
                camAngle = Mathf.Clamp(camAngle, minAngle, maxAngle);
                Debug.Log(camAngle + " CamAngle");
                camAnchor.transform.localRotation = Quaternion.Euler(0, camAngle, 0);
            }
        }

    }
}

