﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{    
    AIManager managerScript;

    NavMeshAgent agent;    
    state AgentState= state.idle;
    int targetID;
    Transform target;
    Animator anim;
    int counter = 0;
    enum state {wander, talking, idle, dead}
        
    
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        managerScript = GameObject.Find("AI manager").GetComponent<AIManager>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {        
        if (AgentState==state.idle)
        {
            Wander();
        }

        if (AgentState==state.dead)
        {            
            Invoke("Die", 5); //Destroys agent after 5 seconds
        }
    }

    private void Wander()
    {
        System.Random rnd = new System.Random();
        int tempValue=targetID;

        do //Prevent picking same target
        {
            targetID = rnd.Next(0, 26);
        } while (targetID==tempValue);
        
        
        switch (targetID) //Set target
        {
            case 0:
                agent.SetDestination(managerScript.target0.position);
                break;

            case 1:
                agent.SetDestination(managerScript.target1.position);
                break;

            case 2:
                agent.SetDestination(managerScript.target2.position);
                break;

            case 3:
                agent.SetDestination(managerScript.target3.position);
                break;
            case 4:
                agent.SetDestination(managerScript.target4.position);
                break;
            case 5:
                agent.SetDestination(managerScript.target5.position);
                break;
            case 6:
                agent.SetDestination(managerScript.target6.position);
                break;
            case 7:
                agent.SetDestination(managerScript.target7.position);
                break;
            case 8:
                agent.SetDestination(managerScript.target8.position);
                break;
            case 9:
                agent.SetDestination(managerScript.target9.position);
                break;
            case 10:
                agent.SetDestination(managerScript.target10.position);
                break;
            case 11:
                agent.SetDestination(managerScript.target11.position);
                break;
            case 12:
                agent.SetDestination(managerScript.target12.position);
                break;
            case 13:
                agent.SetDestination(managerScript.target13.position);
                break;
            case 14:
                agent.SetDestination(managerScript.target14.position);
                break;
            case 15:
                agent.SetDestination(managerScript.target15.position);
                break;
            case 16:
                agent.SetDestination(managerScript.target16.position);
                break;
            case 17:
                agent.SetDestination(managerScript.target17.position);
                break;
            case 18:
                agent.SetDestination(managerScript.target18.position);
                break;
            case 19:
                agent.SetDestination(managerScript.target19.position);
                break;
            case 20:
                agent.SetDestination(managerScript.target20.position);
                break;
            case 21:
                agent.SetDestination(managerScript.target21.position);
                break;
            case 22:
                agent.SetDestination(managerScript.target22.position);
                break;
            case 23:
                agent.SetDestination(managerScript.target23.position);
                break;
            case 24:
                agent.SetDestination(managerScript.target24.position);
                break;
            case 25:
                agent.SetDestination(managerScript.target25.position);
                break;

            default:
                break;
        }
        AgentState = state.wander;
        anim.SetInteger("condition", 1); //Activate walk animation
        //Debug.Log(targetID);        
    }
    
    private void Idle()
    {
        agent.isStopped = false;
        AgentState = state.idle;
        anim.SetBool("isInteracting", false); //Deactivate interacting animation
        anim.SetBool("isTalking", false); //Deactivate talking animation
        anim.SetInteger("condition", 0); //Activate idle animation
    }

    private void Die()
    {        
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (this.tag== "26")//Checks if agent is the double-agent
        {
            //Debug.Log("i'm the double agent");
            if (other.tag=="entity")
            {
                //Debug.Log("hello!");
                agent.isStopped = true;
                anim.SetBool("isTalking", true);                
                Invoke("Idle", 6); //Wait 6 seconds to go back to idling
            }
        }
        if (other.tag == "26") // Agent talks with double agent
        {
            agent.isStopped = true;
            anim.SetBool("isTalking", true);
            Invoke("Idle", 6);

        }
        if (other.gameObject.tag == targetID.ToString())
        {
            if (targetID<=3) //Interact with target
            {
                //Debug.Log("action required");
                anim.SetBool("isInteracting", true); //Activate interacting animation
                Invoke("Idle", 5); //Wait 5 seconds to go back to idling
            }
            else
            {
                //Debug.Log("Colision detected");
                Idle();
            }
        }
        if (this.gameObject.GetComponent<Collider>() == MouseLookScript.hitCol) //Condition to die goes here
        {
            AgentState = state.dead;
            agent.isStopped = true;
            anim.SetBool("isDead", true);
        }
    }
}
