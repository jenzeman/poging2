﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadMe : MonoBehaviour
{
    [SerializeField]
    private GameObject readMe;


    public void OpenReadMe()
    {
        readMe.SetActive(true);
    }

    public void CloseReadMe()
    {
        readMe.SetActive(false);
    }
}

