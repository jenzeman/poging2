﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BlackStart : MonoBehaviour
{
    public Texture tex, startTex;
    bool alreadySwitched;

    // Start is called before the first frame update
    void Start()
    {
        alreadySwitched = false;

    }

    // Update is called once per frame
    void Update()
    {
        //Show a blackscreen until infiltrant player has joined the room

        if (!alreadySwitched)
        {

            if (GameObject.Find("character(Clone)") == null)
            {
                this.GetComponent<RawImage>().texture = startTex; //Set texture to either image or a texture
            }
            else
            {
                this.GetComponent<RawImage>().texture = tex;
                alreadySwitched = true;
            }

        }
    }
}
