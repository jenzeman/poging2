﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIManager : MonoBehaviour
{
    public GameObject aiPrefab;
    public int numberOfAgents = 5;

    public Transform target0;
    public Transform target1;
    public Transform target2;
    public Transform target3;
    public Transform target4;
    public Transform target5;
    public Transform target6;
    public Transform target7;
    public Transform target8;
    public Transform target9;
    public Transform target10;
    public Transform target11;
    public Transform target12;
    public Transform target13;
    public Transform target14;
    public Transform target15;
    public Transform target16;
    public Transform target17;
    public Transform target18;
    public Transform target19;
    public Transform target20;
    public Transform target21;
    public Transform target22;
    public Transform target23;
    public Transform target24;
    public Transform target25;

    public Transform spawnPoint0;
    public Transform spawnPoint1;
    public Transform spawnPoint2;
    public Transform spawnPoint3;
    public Transform spawnPoint4;
    public Transform spawnPoint5;

    public Material playerMaterial;

    public Material white;
    public Material blue;
    public Material green;
    public Material red;
    public Material yellow;
    public Material purple;

    public Color doubleAgentColor;
    // Start is called before the first frame update
    void Start()
    {
        System.Random rnd = new System.Random();
        Transform[] spawnPoints = { spawnPoint0, spawnPoint1, spawnPoint2, spawnPoint3, spawnPoint4, spawnPoint5 };
        Material[] materials = { white, blue, green, red, yellow, purple };

        //Randomize spawnpoints        
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            SwapTransform(spawnPoints, i, i + rnd.Next(spawnPoints.Length - i));
        }

        //Randomize character colors        
        for (int i = 0; i < materials.Length; i++)
        {
            SwapMaterial(materials, i, i + rnd.Next(spawnPoints.Length - i));
        }

        //Spawn ai agents
        for (int i = 0; i < numberOfAgents; i++)
        {
            aiPrefab.GetComponentInChildren<SkinnedMeshRenderer>().material = materials[i];
            var agent = Instantiate(aiPrefab, spawnPoints[i].position, Quaternion.identity);

            //The first agent that spawns is the double agent
            if (i == 0)
            {
                agent.name = "DoubleAgent";
                agent.tag = "26";
                if (materials[i] == white)
                {
                    doubleAgentColor = Color.white;
                }
                if (materials[i] == blue)
                {
                    doubleAgentColor = Color.blue;
                }
                if (materials[i] == green)
                {
                    doubleAgentColor = Color.green;
                }
                if (materials[i] == red)
                {
                    doubleAgentColor = Color.red;
                }
                if (materials[i] == yellow)
                {
                    doubleAgentColor = Color.yellow;
                }
                if (materials[i] == purple)
                {
                    doubleAgentColor = Color.magenta;
                }
            }
        }

        //Set player color
        playerMaterial = materials[5];

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void SwapTransform(Transform[] array, int a, int b) //Used to randomize spawnpoints
    {
        Transform temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
    private void SwapMaterial(Material[] array, int a, int b) //Used to randomize spawnpoints
    {
        Material temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }
}
